use anyhow::{anyhow, bail, Context, Error};
use cidr::{Cidr, Ipv6Cidr};
use crimp::Request;
use curl::easy::SeekResult;
use fork::Fork;
use log::LevelFilter;
use log::*;
use minidom::{Element, NSChoice};
use serde_derive::{Deserialize, Serialize};
use std::io::{self, Read};
use std::process::Command;
use syslog::{BasicLogger, Facility, Formatter3164};
use tinytemplate::TinyTemplate;

pub static CONFIG_PATH: &'static str = "/etc/libvirt-vnet.toml";

#[derive(Deserialize)]
pub struct Config {
    /// URL of the endpoint to use for querying prefix allocations.
    allocation_endpoint: String,
    /// A template radvd.conf to use.
    radvd_template: String,
    /// Directory to place generated radvd.conf files in.
    radvd_out_dir: String,
    /// systemd unit name for radvd.
    radvd_unit: String,
}

#[derive(Debug)]
pub struct HookData {
    pub object_name: Option<String>,
    pub operation: Option<String>,
    pub sub_operation: Option<String>,
    pub extra: Option<String>,
}
impl HookData {
    fn dash_to_none(val: String) -> Option<String> {
        if val == "-" {
            None
        } else {
            Some(val)
        }
    }
    pub fn from_env() -> Self {
        let mut args = std::env::args();
        let _executable_name = args.next();

        HookData {
            object_name: args.next().and_then(Self::dash_to_none),
            operation: args.next().and_then(Self::dash_to_none),
            sub_operation: args.next().and_then(Self::dash_to_none),
            extra: args.next().and_then(Self::dash_to_none),
        }
    }
}

#[derive(Deserialize)]
pub struct PrefixAllocation {
    pub prefix: Option<String>,
}
impl PrefixAllocation {
    pub fn get(endpoint: &str, dom_name: &str) -> Result<Option<Ipv6Cidr>, Error> {
        let resp = Request::get(&format!("{}?domain={}", endpoint, dom_name))
            .with_handle(|hdl| hdl.follow_location(true))?
            .send()?
            .error_for_status(|r| anyhow!("failed with code {}", r.status))?;
        let allox: Self = resp.as_json()?.body;
        let cidr = allox.prefix.map(|x| x.parse::<Ipv6Cidr>()).transpose()?;
        Ok(cidr)
    }

    pub fn create(endpoint: &str, dom_name: &str) -> Result<Ipv6Cidr, Error> {
        let resp = Request::post(&format!("{}?domain={}", endpoint, dom_name))
            .with_handle(|hdl| hdl.follow_location(true))?
            .with_handle(|hdl| hdl.seek_function(|_| SeekResult::Ok))?
            .send()?
            .error_for_status(|r| anyhow!("failed with code {}", r.status))?;
        let allox: Self = resp.as_json()?.body;
        let cidr = allox
            .prefix
            .ok_or_else(|| anyhow!("prefix somehow not allocated on request"))?;
        Ok(cidr.parse()?)
    }
}

#[derive(Serialize)]
pub struct RadvdContext {
    pub ifname: String,
    pub prefix: String,
}

pub struct TapDevice {
    pub ifname: String,
    pub mac_addr: Option<String>,
}

pub struct DomainInformation {
    pub name: String,
    pub uuid: String,
    pub devices: Vec<TapDevice>,
}

impl DomainInformation {
    fn get(domain: &Element) -> Result<Self, Error> {
        let dom_name = domain
            .get_child("name", NSChoice::Any)
            .ok_or_else(|| anyhow!("no <name> element in <domain>"))?
            .text();
        let dom_uuid = domain
            .get_child("uuid", NSChoice::Any)
            .ok_or_else(|| anyhow!("no <name> element in <domain>"))?
            .text();

        let mut devices = vec![];

        if let Some(d) = domain.get_child("devices", NSChoice::Any) {
            for dev in d.children() {
                if dev.name() == "interface" && dev.attr("type") == Some("ethernet") {
                    let target = dev
                        .get_child("target", NSChoice::Any)
                        .ok_or_else(|| anyhow!("no <target> child of device {:?}", dev))?;
                    let ifname = target.attr("dev").ok_or_else(|| {
                        anyhow!("<target> element has no interface name: {:?}", target)
                    })?;
                    let mac = dev
                        .get_child("mac", NSChoice::Any)
                        .and_then(|elem| elem.attr("address"))
                        .map(|x| x.to_owned());
                    devices.push(TapDevice {
                        ifname: ifname.to_owned(),
                        mac_addr: mac,
                    });
                }
            }
        };

        Ok(Self {
            name: dom_name,
            uuid: dom_uuid,
            devices,
        })
    }
}

fn run() -> Result<(), Error> {
    let hook = HookData::from_env();
    info!("* hook invoked {:?}", hook);

    match (
        hook.operation.as_ref().map(|x| &x as &str),
        hook.sub_operation.as_ref().map(|x| &x as &str),
    ) {
        (Some("start"), Some("begin")) => {}
        (_, _) => return Ok(()),
    }

    let cfg_path = std::env::var("VNET_CONFIG").unwrap_or_else(|_| CONFIG_PATH.to_string());
    debug!("* reading config at {}", cfg_path);
    let cfg: Config =
        toml::from_str(&std::fs::read_to_string(&cfg_path).context("reading config")?)
            .context("parsing config")?;
    let mut tt = TinyTemplate::new();
    tt.add_template("radvd", &cfg.radvd_template)
        .context("failed to load radvd template")?;
    crimp::init();

    // FIXME(eta): The `minidom` crate requires an XML namespace, so we have this hacky root
    //             element in here.
    let mut dom_xml_raw = "<root xmlns=\"haxx\">".to_string();
    debug!("* reading domain XML");
    io::stdin()
        .read_to_string(&mut dom_xml_raw)
        .context("reading domain XML")?;
    dom_xml_raw.push_str("</root>");
    debug!("* parsing domain XML");
    let dom_xml: Element = dom_xml_raw.parse().context("parsing domain XML")?;
    let domain = dom_xml
        .get_child("domain", NSChoice::Any)
        .ok_or_else(|| anyhow!("no <domain> element in parsed XML"))?;
    debug!("* extracting domain information");
    let info = DomainInformation::get(domain).context("extracting domain information failed")?;
    info!(
        "* found domain '{}', UUID {}, with {} relevant network interfaces",
        info.name,
        info.uuid,
        info.devices.len()
    );
    if info.devices.len() != 1 {
        info!("* expecting exactly one interface; exiting");
        return Ok(());
    }
    let ifname = &info.devices[0].ifname;
    let systemd_service = format!("{}@{}.service", cfg.radvd_unit, ifname);
    debug!("* querying for allocation");
    let mut cidr = PrefixAllocation::get(&cfg.allocation_endpoint, &info.name)
        .context("failed to query for allocation")?;
    if cidr.is_none() {
        warn!("* no allocation exists, requesting creation");
        cidr = Some(
            PrefixAllocation::create(&cfg.allocation_endpoint, &info.name)
                .context("failed to request new allocation")?,
        );
    }
    let cidr = cidr.ok_or_else(|| anyhow!("prefix not allocated?"))?;
    info!("* got prefix {}", cidr);
    let host_addr = cidr
        .iter()
        .nth(1)
        .ok_or_else(|| anyhow!("cronched prefix"))?;
    let host_cidr = format!("{}/{}", host_addr, cidr.network_length());
    debug!("* templating radvd configuration");
    let radvd_conf = tt
        .render(
            "radvd",
            &RadvdContext {
                ifname: ifname.to_string(),
                prefix: cidr.to_string(),
            },
        )
        .context("failed to render radvd config")?;
    let out_path = format!("{}/{}.conf", cfg.radvd_out_dir, ifname);
    info!("* writing radvd configuration to {}", out_path);
    std::fs::write(out_path, radvd_conf)?;
    info!("* forking");
    if let Ok(Fork::Child) = fork::daemon(true, false) {
        info!("* engaging awful tcpdump hack");
        let stat = Command::new("tcpdump")
            .args(&["-Q", "in", "-c", "1", "-n", "-i", ifname])
            .status()?;
        if !stat.success() {
            warn!("tcpdump failed with code {:?}", stat.code());
        }
        info!("* adding address {} to {}", host_cidr, ifname);
        let stat = Command::new("ip")
            .args(&["addr", "add", &host_cidr, "dev", ifname])
            .status()?;
        if !stat.success() {
            bail!("iproute2 failed with code {:?}", stat.code());
        }
        info!("* starting radvd service {}", systemd_service);
        let stat = Command::new("systemctl")
            .args(&["restart", &systemd_service])
            .status()?;
        if !stat.success() {
            bail!("systemctl failed with code {:?}", stat.code());
        }
    }
    Ok(())
}

fn main() -> Result<(), Error> {
    let formatter = Formatter3164 {
        facility: Facility::LOG_USER,
        hostname: None,
        process: "libvirt-vnet".into(),
        pid: 0,
    };

    let logger =
        syslog::unix(formatter).map_err(|e| anyhow!("could not connect to syslog: {}", e))?;
    log::set_boxed_logger(Box::new(BasicLogger::new(logger)))
        .map(|()| log::set_max_level(LevelFilter::Info))?;
    if let Err(e) = run() {
        error!("fatal: {:#}", e);
        return Err(e);
    }
    Ok(())
}
